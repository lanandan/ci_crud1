<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CreateNewUser extends CI_Controller{

	
	public function index()
	{
		$data['messages']="Test Create New User Index";
		$this->load->view('header');
		$this->load->view('createnewuser',$data);
		$this->load->view('footer');
	}

	public function create()
	{

		if($this->input->post('un')!="" && $this->input->post('pwd')!="")
		{
			$data['username']=$this->input->post('un');
			$data['password']=$this->input->post('pwd');

			$data['firstname']=$this->input->post('firstname');
			$data['lastname']=$this->input->post('lastname');

			$mon=$this->input->post('mon');
			$day=$this->input->post('day');
			$year=$this->input->post('year');

			$data['dob']=$year."-".$mon."-".$day;

			$data['gender']=$this->input->post('gender');

			$this->user_model->insert_new_user($data);

			redirect('?create=sucess');
		}
	
	}

	public function edituser()
	{
		$data['entry'] =  $this->user_model->get_entry($this->uri->segment(3, 0));
		if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
			echo "error";
		}
		else
		{
			$data['entry'] = $data['entry'][0];
			$this->load->view('header');
			$this->load->view('edituser', $data);
			$this->load->view('footer');
		}
	}
	
	public function updateuser()
	{
		
		if($this->input->post('un')!="" && $this->input->post('pwd')!="" && $this->input->post('id') != "")
		{
			$data['username']=$this->input->post('un');
			$data['password']=$this->input->post('pwd');
	
			$data['id'] = $this->input->post('id');

			$this->user_model->update_user($data);

			redirect('?create=sucess');
		}
	}
}


?>
