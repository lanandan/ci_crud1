<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller 
{

	public function index()
	{
		$data['user'] = "";
		$this->load->view('header');
		$this->load->view('login');
		$this->load->view('footer');
	}

	public function login_check()
	{
		if($this->input->post('un')!="" && $this->input->post('pwd')!="")
		{
			$data['username']=$this->input->post('un');
			$data['password']=$this->input->post('pwd');

			$data['entry'] = $this->user_model->check_login_user($data);

			if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
				echo "error";
			}
			else
			{
				$data['entry'] = $data['entry'][0];

				$this->session->set_userdata($data);

				$this->userhome($data); 
			}

			//redirect('?create=sucess');
		}

	}

	public function userhome($data)
	{

		//$this->session->all_userdata()


		$this->load->view('header');
		$this->load->view('userhome', $data);
		$this->load->view('footer');
	}

	public function logout_check()
	{


	}


}

?>
