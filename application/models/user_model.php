<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

	var $id="";
	var $username="";
	var $password="";
	var $firstname="";
	var $lastname="";
	var $dob="";
	var $gender="";

	function __construct()
    	{
        	// Call the Model constructor
        	parent::__construct();
    	}

	function insert_new_user($data)
	{
		$this->username=$data['username'];
		$this->password=$data['password'];

		$this->firstname=$data['firstname'];
		$this->lastname=$data['lastname'];
		$this->dob=$data['dob'];
		$this->gender=$data['gender'];

		$this->db->insert('user',$this);
	}
	
	function get_all_users()
	{
		$query=$this->db->get('user');
		return $query->result();
	}

	function get_entry($id)
	{
		$this->db->where('id', $id);
		$query = $this->db->get('user', 1);
		return $query->result();
        }

	function update_user($data)
	{
		$this->username=$data['username'];
		$this->password=$data['password'];

		$this->id=$data['id'];

		$this->db->update('user', $this, array('id' => $data['id']));
	}

	function delete_entry($id)
	{
        	$this->db->delete('user', array('id' => $id));
    	}

	// for login 

	function check_login_user($data)
	{
		$this->username=$data['username'];
		$this->password=$data['password'];

		$sql="select * from user where username='$this->username' AND password='$this->password'";

		//$this->db->where($where);
		$query = $this->db->query($sql);
		return $query->result();
	}

}
?>
