<br><br><br>
<style type="text/css">
    body { padding-top:30px; }
    .form-control { margin-bottom: 10px; }    
</style>
<script type="text/javascript">
        window.alert = function(){};
        var defaultCSS = document.getElementById('bootstrap-css');
        function changeCSS(css){
            if(css) $('head > link').filter(':first').replaceWith('<link rel="stylesheet" href="'+ css +'" type="text/css" />'); 
            else $('head > link').filter(':first').replaceWith(defaultCSS); 
        }
        $( document ).ready(function() {
          var iframe_height = parseInt($('html').height()); 
          window.parent.postMessage( iframe_height, 'http://test.com');
        });
</script>

<div id="body">

    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4 well well-sm">
            <legend><a href="#"><i class="glyphicon glyphicon-globe"></i></a> Sign up!</legend>
            <form method="post" class="form" role="form" action="http://localhost/CISamples/CRUD2/index.php/createnewuser/create/">
            <div class="row">
                <div class="col-xs-6 col-md-6">
                    <input class="form-control" name="firstname" placeholder="First Name" type="text" required autofocus />
                </div>
                <div class="col-xs-6 col-md-6">
                    <input class="form-control" name="lastname" placeholder="Last Name" type="text" required />
                </div>
            </div>
            <input class="form-control" name="un" placeholder="Username" type="text" required />
            <input class="form-control" name="pwd" placeholder="Password" type="password" required />
            <label for="">
                Birth Date</label>
            <div class="row">
                <div class="col-xs-4 col-md-4">
                    <select class="form-control" name="mon">
                        <option value="Month">Month</option>
			<?php
				for($i=1;$i<=12;$i++)
					echo "<option value='$i'>".$i."</option>";
			?>
                    </select>
                </div>
                <div class="col-xs-4 col-md-4">
                    <select class="form-control" name="day">
                        <option value="Day">Day</option>
			<?php
				for($i=1;$i<=31;$i++)
					echo "<option value='$i'>".$i."</option>";
			?>
                    </select>
                </div>
                <div class="col-xs-4 col-md-4">
                    <select class="form-control" name="year">
                        <option value="Year">Year</option>
			<?php
				for($i=1980;$i<=date('Y');$i++)
					echo "<option value='$i'>".$i."</option>";
			?>
                    </select>
                </div>
            </div>
            <label class="radio-inline">
                <input type="radio" name="gender" id="inlineCheckbox1" value="male" />
                Male
            </label>
            <label class="radio-inline">
                <input type="radio" name="gender" id="inlineCheckbox2" value="female" />
                Female
            </label>
            <br />
            <br />
            <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit">
                Sign up</button>
            </form>
        </div>
    </div>


</div>
<br><br><br>
<br><br><br>
   


